# Nerdearla - Integrando el mundo con Kafka & Friends

Proyecto destinado a contener material del workshop dictado para Nerdearla, cuenta con todo lo requerido para la ejecucion del workshop.

[Integrando el mundo con Kafka & sus amigos](https://docs.google.com/presentation/d/1fkp46ePLruL1LHXjmZPiDllYpcr-_OPKntWjIGH8bwU/edit?usp=sharing)

## Proyectos usados.

- [Docker](https://docs.docker.com/desktop) // [Podman](https://podman.io/getting-started/installation)
- [Docker-compose](https://docs.docker.com/compose/install/) //  [Podman-compose](https://github.com/containers/podman-compose)
- [Drivers JDBC Confluent](https://docs.confluent.io/kafka-connect-jdbc/current/index.html), incluye fat jar
- [Google PubSub](https://github.com/GoogleCloudPlatform/pubsub.git), incluye [fat Jar](https://github.com/GoogleCloudPlatform/pubsub/releases) 


## Overview

Este proyecto contiene todo lo requerido para montar localmente :
- broker Kafka
- broker RabbitMQ
- Instancia de kafka Connect Distribuida
- Plugins : JDBC , GCP , RabbitMQ-Sink , JsonPath , Apache/Confluent Transformations
- Connectors JDBC , GCP y RabbitMQ
- Instancia de BD MSSQL Server 2017

### Folders

```
kc-nerdearla/
├── docker-compose.yml
├── plugins
│   ├── gcp
│   │   └── pubsub-kafka-connector.jar
│   ├── jdbc
│   │   ├── jtds-1.3.1.jar
│   │   └── kafka-connect-jdbc-10.3.x-SNAPSHOT-jar-with-dependencies.jar
│   ├── rabbit
│   │   └── kafka-connect-connector-4rabbitmq-1.0.0.jar
│   └── transforms
│       ├── accessors-smart-1.2.jar
│       ├── asm-5.0.4.jar
│       ├── connect-transforms-1.3.2.jar
│       ├── connect-utils-0.1.0.jar
│       ├── hamcrest-core-1.3.jar
│       ├── hamcrest-library-1.3.jar
│       ├── joda-time-2.10.3.jar
│       ├── json-path-2.4.0.jar
│       ├── json-smart-2.3.jar
│       └── slf4j-api-1.7.25.jar
├── rabbitmq
│   ├── data
│   ├── etc
│   │   └── rabbitmq.conf
│   └── logs
└── restfulapi
    └── kc-nerdearla.postman_collection.json
```

## Uso

Para inicializar los contenedores pueden usar docker/docker-compose o podman, los scripts son los mismos.

```
docker-compose up -d
```
```
podman-compose up -d
```

* Linux : deben desactivar SElinux y ejecutar con su/sudo
    *   ```
        sudo setenforce 0 
        ```

## Scripts recurrentes.

Para podman, deben cambiar "docker-compose" -> "podman-compose" 

### Todos los contenedores : Inicio / detención + eliminación 
```
docker-compose up -d
```
```
docker-compose down
```

### Conexion a un contenedor
```
docker exec -it EL_CONTENEDOR bin/bash
```


### Consumo de eventos 
```
docker-compose exec kbroker kafka-console-consumer --topic TOPIC_TEST --bootstrap-server kbroker:9091 --from-beginning
```

### Produccion de eventos
```
docker-compose exec kbroker kafka-console-producer --topic TOPIC_TEST --broker-list kbroker:9091
```

## Scripts Workshop

Los scripts del workshop estan incluidos en la presentación.

[Integrando el mundo con Kafka & sus amigos](https://docs.google.com/presentation/d/1fkp46ePLruL1LHXjmZPiDllYpcr-_OPKntWjIGH8bwU/edit?usp=sharing)

